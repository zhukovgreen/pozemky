# POZEMEK
POZEMEK from Czech is a piece of land. 
The application purpose is tracking down web sites
where new lands could be posted for a sale and notify 
the user via telegram messenger.

API documentation can be found at 
`$HOST:$PORT/api/doc`

## Common steps
1. git clone https://gitlab.com/zhukovgreen/pozemky.git
1. rename `.env.example` to `.env` and fill values for
`BOT_TOKEN` and `RECIPIENT_TG_ID`

Example is in `.env.example`
```bash
BOT_TOKEN="your bot token here"
RECIPIENT_TG_ID="your recipient id here"
```

`BOT_TOKEN` could be generated as per 
[instruction](https://core.telegram.org/bots#3-how-do-i-create-a-bot)

`RECIPIENT_TG_ID` could be found as per 
[instruction](https://stackoverflow.com/questions/32683992/find-out-my-own-user-id-for-sending-a-message-with-telegram-api#32777943)

2. Adopt `pozemky_config.yml` to your needs: add URLs
of you interest and corresponding search string (see
`Configuration` chapter)

3. When the ap is up and running you have to send
`http $APP_HOST:$APP_PORT/spawn_trackers` to deploy
url trackers (http is a httpie client)

## Run on local machine
1. docker-compose -f docker-compose.yml -f 
docker-compose.deploy.yml up

## Deploy
1. docker swarm init
1. docker stack deploy -c docker-compose.deploy.yml 
pozemky

or (less serious way):

1. docker-compose -f docker-compose.deploy.yml up

## Dev env
1. pip install pipenv --user
1. pipenv install --dev
1. pipenv shell

## Configuration
Configuration file is stored in `pozemky_config.yml`
The structure is:
```yaml
app:
  debug: True
  max_workers:  5
  update_frequency: '30s'
trackers:
  - url: 'https://www.roznovak.cz/nemovitosti-na-roznovsku'
    search_string: '<a href="/inzerat/{}"'
```
