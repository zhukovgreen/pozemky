"""Application setup."""
import base64
import logging
import os
from logging.config import dictConfig

import aiohttp
import click
import trafaret as t
import yaml
from aiohttp import web
from aiohttp_session import setup
from aiohttp_session.cookie_storage import (
    EncryptedCookieStorage
)
from aiohttp_swagger import setup_swagger
from cryptography import fernet

from .controller import (
    health_check,
    Pozemky,
    TelegramBot,
)
from .utils import convert_to_sec, conv_to_s_regex


log = logging.getLogger(__name__)

CONFIG = t.Dict(
    {
        t.Key("app"): t.Dict(
            {
                t.Key("debug"): t.Bool,
                t.Key("update_frequency"): t.Regexp(
                    conv_to_s_regex
                ),
            }
        ),
        t.Key("trackers"): t.List(
            t.Dict(
                {
                    t.Key("url"): t.String,
                    t.Key("search_string"): t.String,
                }
            )
        ),
    }
)


def configure_logging(level: str = "INFO") -> None:
    """Configure logging at a given level.

    :param level: could be INFO, DEBUG etc.
    """
    dictConfig(
        {
            "version": 1,
            "formatters": {
                "standard": {
                    "format": "%(asctime)s [%(levelname)s] %(name)s: %(message)s"
                }
            },
            "handlers": {
                "default": {
                    "level": level,
                    "class": "logging.StreamHandler",
                    "stream": "ext://sys.stdout",
                    "formatter": "standard",
                },
                "to_file": {
                    "level": level,
                    "class": "logging.handlers.RotatingFileHandler",
                    "maxBytes": 10485760,
                    "backupCount": 10,
                    "filename": "./logs/pozemky.log",
                    "formatter": "standard",
                },
            },
            "loggers": {
                "pozemky": {
                    "handlers": [
                        "default",
                        "to_file",
                    ],
                    "level": level,
                    "propagate": False,
                }
            },
        }
    )
    log.info(f"Logger level set to {level}")


@click.group()
def cli() -> None:
    """Cli entrypoint."""
    pass


def build_application() -> web.Application:
    """Build application.

    register routes, configure logging.
    """
    with open("pozemky_config.yml") as config_f:
        config = CONFIG.check(
            yaml.safe_load(config_f)
        )

    configure_logging(
        level="DEBUG"
        if config["app"]["debug"]
        else "INFO"
    )
    log.info("Building application")
    app = aiohttp.web.Application()
    fernet_key = fernet.Fernet.generate_key()
    secret_key = base64.urlsafe_b64decode(fernet_key)
    setup(app, EncryptedCookieStorage(secret_key))

    tg_bot = TelegramBot(
        token=os.environ.get(
            "BOT_TOKEN",
            "564817124:AAFvhYsH1L3TKLEAD9b"
            "f_FIyz1gZsNj5JeI",
        ),
        recipient_tg_id=os.environ.get(
            "RECIPIENT_TG_ID", "564817124"
        ),
    )

    pozemky = Pozemky(
        trackers=config["trackers"],
        notification_bot=tg_bot,
        update_frequency=convert_to_sec(
            config["app"]["update_frequency"]
        ),
    )

    log.info("Registering endpoints")
    app.add_routes(
        [
            web.get("/health_check", health_check),
            web.get(
                "/spawn_trackers",
                pozemky.spawn_trackers,
            ),
        ]
    )
    setup_swagger(
        app,
        description=(
            "Looks for a patterns in the "
            "web pages and if the pattern changes it "
            "sends the notification to the user via "
            "telegram bot."
        ),
        api_version="1.0.0",
        contact="zhukovgreen@yandex.ru",
    )
    return app


@cli.command()
@click.option(
    "--host", default="127.0.0.1", help="Server host"
)
@click.option(
    "--port", default="5000", help="Server port"
)
def run_server(host: str, port: int) -> None:
    """Runs the f_air server at `host` and `port`.

    Example:
    >>> run_server(host='0.0.0.0', port=5000)
    """
    app = build_application()
    log.info(f"Starting app at http://{host}:{port}")
    log.info(
        "Api docs see at "
        f"http://{host}:{port}/api/doc"
    )
    command: bash
    aiohttp.web.run_app(
        app, host=host, port=port, access_log=log
    )
