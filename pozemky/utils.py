import re


conv_to_s_regex = r"^(\d+[smhdw] )*(\d+[smhdw]){1}$"


def convert_to_sec(time_str: str) -> int:
    """Convert a string format of the time input into seconds.

    Example:
    >>> convert_to_sec('1h')
    3600
    >>> convert_to_sec('1h 1s')
    3601
    >>> convert_to_sec('1w 1h 1m 1s')
    608461
    """
    seconds_per_unit = {
        "s": 1,
        "m": 60,
        "h": 3600,
        "d": 86400,
        "w": 604800,
    }

    if not re.compile(conv_to_s_regex).match(
        time_str
    ):
        raise ValueError(
            f"time_str doesn`t match the regex"
            f"{conv_to_s_regex}"
        )

    return sum(
        int(a[:-1]) * seconds_per_unit[a[-1]]
        for a in time_str.split()
    )
