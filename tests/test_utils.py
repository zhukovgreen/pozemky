"""Testing utils module."""
import pytest

from pozemky.utils import convert_to_sec


def test_convert_to_sec() -> None:
    assert convert_to_sec("1s") == 1
    assert convert_to_sec("1h 1m 1s") == 3661
    with pytest.raises(ValueError) as err:
        convert_to_sec("1h ")
    assert str(err.value) == (
        "time_str doesn`t match the regex"
        "^(\\d+[smhdw] )*(\\d+[smhdw]){1}$"
    )
