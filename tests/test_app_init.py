"""Testing app initialization.

Logging, end-points registering
"""
import pytest
from _pytest.logging import LogCaptureFixture
from aiohttp.test_utils import TestClient


async def test_app_init(
    app: TestClient, caplog: LogCaptureFixture
) -> None:
    with caplog.at_level(
        "INFO", logger="pozemky.app"
    ):
        resp = await app.get("/health_check")
    assert resp.status == 200
    # FIXME assert caplog.text == "Gh"
