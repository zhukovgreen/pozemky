"""Test functionality of the telegram bot."""
import pytest
from telegram.error import Unauthorized

from pozemky.controller import TelegramBot


@pytest.fixture
def bot() -> TelegramBot:
    """Get an instance of a bot."""
    return TelegramBot(
        token=(
            "564817124:AAFvhYsH1L3TKL"
            "EAD9bf_FIyz1gZsNj5JeI"
        ),
        recipient_tg_id="564817124",
    )


def test_send_msg(bot: TelegramBot) -> None:
    """Test if bot sends the messages.

    Sending the msg from bot to bot is forbidden,
    thus we check the error msg.
    """
    with pytest.raises(Unauthorized) as err:
        bot.send_msg("Hi")
    assert err.exconly() == (
        "telegram.error.Unauthorized: Forbidden: "
        "bot can't send messages to bots"
    )
