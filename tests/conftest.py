"""Pytest fixtures place."""
import sys
from asyncio.unix_events import SelectorEventLoop

import pytest
from aiohttp import web
from aiohttp.test_utils import TestClient

sys.path.insert(0, ".")

from pozemky.app import build_application


@pytest.fixture
def app(
    loop: SelectorEventLoop,
    aiohttp_client: TestClient,
) -> web.Application:
    return loop.run_until_complete(
        aiohttp_client(build_application())
    )
