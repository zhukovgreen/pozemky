"""Tests for pozemky.controller Pozemky.bind_watcher."""
from pytest_mock import MockFixture

from pozemky.controller import Pozemky


async def test_main(mocker: MockFixture) -> None:
    """Test bind_watcher method of Pozemky.

    `bind_watcher` is a daemon which is run in the
    executor. We test that in 2 cycles, notification
    bot sending the message only once, because second
    time the pattern remains the same and we shouldn't
    be notified.
    """
    bot = mocker.Mock()
    search_patterns = mocker.Mock()
    update_frequency = 2

    pozemky = Pozemky(
        trackers=search_patterns,
        notification_bot=bot,
        update_frequency=update_frequency,
    )

    await pozemky.run_tracker(
        url=(
            "https://www.roznovak.cz/"
            "nemovitosti-na-roznovsku"
        ),
        pattern='<a href="/inzerat/{}"',
        max_cycles=2,
    )

    bot.send_msg.assert_called_once_with(
        "Find new property on:\nhttps://www.roznovak"
        ".cz/nemovitosti-na-roznovsku"
    )
