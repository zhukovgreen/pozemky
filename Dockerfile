FROM python:3.6.5
ENV LANG=C.UTF-8

EXPOSE 5000

WORKDIR /app

RUN pip install pipenv==2018.05.18

COPY . .

# Install pipenv and deploy dev environment
RUN pipenv install --dev --deploy --system
RUN pip install -e .

CMD ["pozemky", "run_server", \
     "--host", "0.0.0.0", "--port", "5000"]
