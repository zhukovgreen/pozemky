from setuptools import setup, find_packages


setup(
    name="pozemky",
    use_scm_version=True,
    packages=find_packages(exclude=["tests", "logs"]),
    entry_points={
        "console_scripts": ["pozemky=pozemky.app:cli"]
    },
    install_requires=["click"],
)
